package com.example.finalurisapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val botttomNavView = findViewById<BottomNavigationView>(R.id.bottom_nav_menu)
        val controller = findNavController(R.id.nav_host_fragment)

        val fragmentSet = setOf<Int>(
            R.id.firstFragment,
            R.id.secondFragment
        )

        setupActionBarWithNavController(controller, AppBarConfiguration(fragmentSet))
        botttomNavView.setupWithNavController(controller)
    }


}