package com.example.finalurisapp

data class Person(
    val id: Int,
    val imageUrl: String,
    val name: String,
    val price: String,
    val contact: String,
)


