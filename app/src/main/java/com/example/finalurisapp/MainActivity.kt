package com.example.finalurisapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var loginButton: Button
    private lateinit var registrationButton: Button
    private lateinit var passwordResetButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerListeners()
    }
    private fun init() {
        editTextEmail = findViewById(R.id.edit_text_email)
        editTextPassword = findViewById(R.id.editTextTextPassword)
        loginButton = findViewById(R.id.login_button)
        registrationButton = findViewById(R.id.registration_button)
        passwordResetButton = findViewById(R.id.password_reset_button)
    }
    private fun registerListeners(){
        registrationButton.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }
        passwordResetButton.setOnClickListener {
            startActivity(Intent(this, PasswordResetActivity::class.java))
        }

        loginButton.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "შეავსეთ ყველა ველი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        gotoProfile()
                    }else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
    private fun gotoProfile(){
        startActivity(Intent(this, ProfileActivity::class.java))
    }


}