package com.example.finalurisapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class CarsActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cars)
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RecyclerViewPersonAdapter(getPersons())
    }

    private fun getPersons() : List<Person>{
        val personList = ArrayList<Person>()

        personList.add(
            Person(
                id = 1,
                imageUrl = "https://di-uploads-pod21.dealerinspire.com/rairdonsmaseratiofkirkland/uploads/2020/09/mc20-nero-enigma-e1601108240742.png",
                name = "Maserati mc20 black",
                price = "Price: 210 000 $",
                contact = "Contact: 599 11 11 00"
            )

        )
        personList.add(
            Person(
                id = 2,
                imageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/BMW_M3_-_panoramio.jpg/1200px-BMW_M3_-_panoramio.jpg?fbclid=IwAR0ZQi6SXmbckY-M7FLpRHf4Hvf_cNlCtpp_j8VFSW1GXgccoOMKIybRtrk",
                name = "BMW M3",
                price = "Price: 69 000 $",
                contact = "Contact: 577 00 00 00"
            )

        )
        personList.add(
            Person(
                id = 3,
                imageUrl = "https://i.pinimg.com/736x/56/9d/1e/569d1e3b06185d4ef988889e28f62ffb.jpg?fbclid=IwAR2bLqaJtAuTxy4Y7c-wDo3aiWkdjdRpWgA7dJOxYT4V-jI7QnSMSQGWFu4",
                name = "Mercedes G Wagon",
                price = "Price: 131 000 $",
                contact = "Contact: 599 69 69 69"
            )

        )
        personList.add(
            Person(
                id = 4,
                imageUrl = "https://c4.wallpaperflare.com/wallpaper/5/369/127/machine-auto-before-lada-wallpaper-preview.jpg?fbclid=IwAR3BU-o-Ge9CQByxeaodUSIeHCitiJzWc7ZRw-XxJ5uUlc8ah88U9R_ER60",
                name = "Lada",
                price = "Price: 1800 $",
                contact = "Contact: 598 41 05 41"
            )

        )
        personList.add(
            Person(
                id = 5,
                imageUrl = "https://ae01.alicdn.com/kf/HTB1NQOSx2iSBuNkSnhJq6zDcpXaH/volga-v12-black-front-view-classic-car-poster-12-x18-Print-Silk-Fabric-art-Wall-Decor.jpg_Q90.jpg_.webp",
                name = "Volga",
                price = "Price: 1500 $",
                contact = "Contact: 551 89 42 12"
            )

        )
        personList.add(
            Person(
                id = 6,
                imageUrl = "https://c4.wallpaperflare.com/wallpaper/449/328/916/ford-ford-mustang-boss-429-black-car-car-muscle-car-hd-wallpaper-preview.jpg?fbclid=IwAR0bRMhF6VDqPLJN425hp8D8-Rvb45yA9I9442l1aqIyh73_XM3jN1AgnBQ",
                name = "Mustang Boss 429",
                price = "Price: 254 000 $",
                contact = "Contact: 599 42 04 20"
            )

        )

        personList.add(
            Person(
                id = 7,
                imageUrl = "https://i.pinimg.com/originals/1c/32/d5/1c32d5b30b0d150bb67db768386ae86e.jpg?fbclid=IwAR2aYHqQTbjDOOVTH5A42T4EeXrC4OYy18vum1liNDgR6jpgzEKzn10m9Fs",
                name = "Mercedes Benc C Class",
                price = "Price: 41 000 $",
                contact = "Contact: 598 19 12 32"
            )

        )

        personList.add(
            Person(
                id = 8,
                imageUrl = "https://i.redd.it/grr0tvzv46v51.jpg",
                name = "Koenigsegg Jesko",
                price = "Price: 3 800 000 $",
                contact = "Contact: 577 66 66 66"
            )

        )
        personList.add(
            Person(
                id = 9,
                imageUrl = "https://cdn.motor1.com/images/mgl/PKZQL/s1/1997-toyota-supra-sold-for-176-000-at-auction.webp?fbclid=IwAR3x8YwUW0SZ9ZojMG0DeKu02_CDAPNxsafbqZpwERfH5AcZamlg2GSqIEw",
                name = "Toyota Supra",
                price = "Price: 15 000 $",
                contact = "Contact: 551 62 82 21"
            )

        )
        personList.add(
            Person(
                id = 10,
                imageUrl = "https://www.mad4wheels.com/img/free-car-images/mobile/2489/nissan-gt-r-black-edition-2009-241417.jpg?fbclid=IwAR1zdi4k-tfRnziN62XKyNDU4BCuyo8VVdmIQ9QIeOquC_GmtsPwKyQD9tA",
                name = "Nissan GTR",
                price = "Price: 130 000 $",
                contact = "Contact: 598 19 84 88"
            )

        )
        personList.add(
            Person(
                id = 11,
                imageUrl = "https://corporate.ferrari.com/sites/ferrari15ipo/files/cover_comunicati/ferrari_f8_tributo_1_1_9.jpg",
                name = "Ferrari F8 Tributo",
                price = "Price: 295 000 $",
                contact = "Contact: 599 01 02 03"
            )

        )
        personList.add(
            Person(
                id = 12,
                imageUrl = "https://www.ccarprice.com/products/Toyota-Prius-Prime-2019.jpg",
                name = "Toyota Prius",
                price = "Price: 24 000 $",
                contact = "Contact: 571 12 26 01"
            )

        )
        personList.add(
            Person(
                id = 13,
                imageUrl = "https://large.shootingsportsmedia.com/809963.jpg?fbclid=IwAR1G0RB60C3j1YHFbT2lKzSuQk1aqIXFGCFpVTnCIkuy1O98Tohq_iPiN24",
                name = "M16A4",
                price = "Price: 1 500 $",
                contact = "Contact: 598 89 26 14"
            )

        )

        personList.add(
            Person(
                id = 14,
                imageUrl = "https://media.istockphoto.com/photos/glock-picture-id89485543?k=20&m=89485543&s=612x612&w=0&h=Dw3d72Kip7TrUNGJf4yTwZFhuplZkDvq9oFsmYRF2KM%3D&fbclid=IwAR3hgWxL2i7L3PEjkoyaUsA0sSO0-cdyNSwfyOdjMzZ7Yk8YZAtjqAZsvic",
                name = "Glock",
                price = "Price: 500 $",
                contact = "Contact: 551 15 65 67"
            )

        )

        personList.add(
            Person(
                id = 15,
                imageUrl = "https://www.gannett-cdn.com/presto/2019/01/22/PMJS/84b25472-4a3b-4ada-95fa-512559b405c9-Uzi_with_scale_1.jpg?fbclid=IwAR21Ltz7DggxAM7Di9Fj4_qEFj0IKPaSZTcJfrk6WbQ74A4j31j0xA8qCD4",
                name = "UZI",
                price = "Price: 1 100 $",
                contact = "Contact: 597 04 64 67"
            )

        )

        personList.add(
            Person(
                id = 16,
                imageUrl = "https://www.pcgamesn.com/wp-content/uploads/2021/11/call-of-duty-warzone-loadout-best-kar98k-vanguard.jpg?fbclid=IwAR1DoRNiku0u5AJ8htOT1ye-v2D9V_SfrqQKo_jA4ELyun24-zzr-PeiVXg",
                name = "Kar98K",
                price = "Price: 780 $",
                contact = "Contact: 555 20 20 12")

        )

        personList.add(
            Person(
                id = 17,
                imageUrl = "https://www.recoilweb.com/wp-content/uploads/2016/11/steyr-aug.jpg?fbclid=IwAR1eO18FUf8oO9zZV4l3ng62aSe8Ifj6NtWrGVjQjonSrL-zctn-4-lu6gw",
                name = "AUG A3",
                price = "Price: 2 200 $",
                contact = "Contact: 598 01 92 73"
            )

        )

        personList.add(
            Person(
                id = 18,
                imageUrl = "https://www.bhphotovideo.com/cdn-cgi/image/format=auto,fit=scale-down,width=500,quality=95/https://www.bhphotovideo.com/images/images500x500/asus_g513qc_bb74_g15_ryzen_7_5800h_1623842445_1633645.jpg?fbclid=IwAR1nZj3wEDqDrK3Sw1auAfsOkTOw3l6iO9sbZLlFrsun8zk11ItTAVzV5pc",
                name = "ASUS 15.6",
                price = "Price: 1 049 $",
                contact = "Contact: 597 23 32 82"
            )

        )

        personList.add(
            Person(
                id = 19,
                imageUrl = "https://www.bhphotovideo.com/cdn-cgi/image/format=auto,fit=scale-down,width=500,quality=95/https://www.bhphotovideo.com/images/images500x500/hp_1v7u4uar_aba_ref_pav_15_i7_1165g7_1617970847_1632751.jpg?fbclid=IwAR3lcDglFX_RwFv7JFJbWiqOb63ifu7Hjq_1CjN2Jtdt6lqJbdK63DCIsz4",
                name = "HP Pavilion 15.6",
                price = "Price: 719 $",
                contact = "Contact: 571 12 26 01"
            )

        )

        personList.add(
            Person(
                id = 20,
                imageUrl = "https://www.bhphotovideo.com/cdn-cgi/image/format=auto,fit=scale-down,width=500,quality=95/https://www.bhphotovideo.com/images/images500x500/acer_nx_a4kaa_003_swift_3_i7_1165g7_8gb_1612356449_1617646.jpg?fbclid=IwAR0cBpSlSU_X2Ne1FyWxonpQWZzbwBvwpopPj7Mypxo4iZ3zvNoef_PrLQA",
                name = "ACER Swift 3",
                price = "Price: 699 $",
                contact = "Contact: 598 19 72 83"
            )

        )

        personList.add(
            Person(
                id = 21,
                imageUrl = "https://www.bhphotovideo.com/cdn-cgi/image/format=auto,fit=scale-down,width=500,quality=95/https://www.bhphotovideo.com/images/images500x500/acer_nx_vpuaa_001_tmp2_i5_1135g7_8gb_512gb_1626825929_1642817.jpg?fbclid=IwAR2CobdaaKR9qZO5vwnNZVj4ud8IBIU57HWMIxoqq_fGFCqI2EcgNc7jfTQ",
                name = "ACER TravelMate P2",
                price = "Price: 650 $",
                contact = "Contact: 555 92 01 21"
            )

        )

        personList.add(
            Person(
                id = 22,
                imageUrl = "https://www.bhphotovideo.com/cdn-cgi/image/format=auto,fit=scale-down,width=500,quality=95/https://www.bhphotovideo.com/images/images500x500/microsoft_10_5_multi_touch_surface_go_1613920820_1626234.jpg?fbclid=IwAR3yUbparPoKb5PkUaIifj9J9ZbQ0u3ECMHWsnvo3oDja7ecJ3uOiR-fVPs",
                name = "Microsoft Multi-Touch ",
                price = "Price: 600 $",
                contact = "Contact: 598 27 16 23"
            )

        )

        personList.add(
            Person(
                id = 23,
                imageUrl = "https://cdn1.it4profit.com/AfrOrF3gWeDA6VOlDG4TzxMv39O7MXnF4CXpKUwGqRM/resize:fill:540/bg:f6f6f6/q:100/plain/s3://catalog-products/201113112918388411/201210170016351443.png@webp?fbclid=IwAR2suymwutmcCNpll7q7GCYlzCYkg1NM0pRizno_nFUH8FVDkFOqAGeO0cU",
                name = "MacBook Air 13 M1",
                price = "Price: 1 899 $",
                contact = "Contact: 551 32 49 12"
            )

        )

        personList.add(
            Person(
                id = 24,
                imageUrl = "https://www.bhphotovideo.com/cdn-cgi/image/format=auto,fit=scale-down,width=500,quality=95/https://www.bhphotovideo.com/images/images500x500/sony_xperia_pro_5g_smartphone_1611832903_1613579.jpg?fbclid=IwAR3JoV7OU9tt_SwOJAlYZfwjswdTxXzeqvwLgW0kGN-htOJHw8yIF3-wGH8",
                name = "Sony Xperia PRO 5G",
                price = "Price: 1 999 $",
                contact = "Contact: 551 23 23 88"
            )

        )
        personList.add(
            Person(
                id = 25,
                imageUrl = "https://cdn1.it4profit.com/AfrOrF3gWeDA6VOlDG4TzxMv39O7MXnF4CXpKUwGqRM/resize:fill:540/bg:f6f6f6/q:100/plain/s3://catalog-products/210915083759249117/210927160050011347.png@webp?fbclid=IwAR3FG3VhVJOQoNIrY4dRf9hjHbTaAUaBqp9F6hN0S1xLiSY2RIk849maceA",
                name = "Iphone 13 mini",
                price = "Price: 1 900 $",
                contact = "Contact: 571 23 47 01"
            )

        )

        personList.add(
            Person(
                id = 27,
                imageUrl = "https://ae04.alicdn.com/kf/H9f10696a11b8402d8106cbf34c01536cq/Chinese-Version-Huawei-P50-Pro-4G-Mobile-Phone-6-6-Inches-OLED-Screen-8GB-256GB-Smart.png_640x640.png?fbclid=IwAR0P-hx0pUQXkWbKBNGHhT5mz_c5n05tB9fLSEku41sAfAcjTDtQIxI7vz0",
                name = "Huawei P50 Pro",
                price = "Price: 2 000 $",
                contact = "Contact: 577 82 21 32"
            )

        )
        personList.add(
            Person(
                id = 28,
                imageUrl = "https://ae04.alicdn.com/kf/H5729d37cbd0c404099476d9fb8f00be0y/Original-google-pixel-4-5-7-P-OLED-snapdragon-855-6gb-ram-64gb-rom-octa-n.jpg_640x640.jpg?fbclid=IwAR0SOgDDEq4iKQtjuGt-0Dw-6nrNmP1MxyJF4bVipkY0dmPYUKSfIFHiVQg",
                name = "Google Pixel 4",
                price = "Price: 340 $",
                contact = "Contact: 578 91 33 82"
            )

        )

        personList.add(
            Person(
                id = 29,
                imageUrl = "https://ae04.alicdn.com/kf/H9bb7b080634a419583b5c02adc13f75dH/Global-Rom-Xiaomi-Mi-Mix-4-5G-NFC-Smartphone-12GB-256GB-Snapdragon-888-Plus-Octa-Core.jpg_640x640.jpg?fbclid=IwAR0GvYutTY7Z6bfz84bnSKp6s7nA7f0IcDdzjwpydPygBAZaX9knpwLSDP4",
                name = "Xiaomi mi mix 5G",
                price = "Price: 1 000 $",
                contact = "Contact: 598 91 82 03"
            )

        )
        personList.add(
            Person(
                id = 30,
                imageUrl = "https://www.notebookcheck.net/fileadmin/Notebooks/News/_nc3/EDIT_6_377.png?fbclid=IwAR21Ltz7DggxAM7Di9Fj4_qEFj0IKPaSZTcJfrk6WbQ74A4j31j0xA8qCD4",
                name = "OnePlus 10 Pro",
                price = "Price: 1 100 $",
                contact = "Contact: 597 82 71 43"
            )

        )
        personList.add(
            Person(
                id = 31,
                imageUrl = "https://img.gkbcdn.com/s3/p/2020-10-19/Global-ROM-OnePlus-8T-6-55-Inch-5G-Smartphone-12GB-256GB-Aquamarine-Gr-426534-0.jpg?fbclid=IwAR3hgWxL2i7L3PEjkoyaUsA0sSO0-cdyNSwfyOdjMzZ7Yk8YZAtjqAZsvic",
                name = "OnePlus 8T",
                price = "Price: 789 $",
                contact = "Contact: 571 12 26 01"
            )

        )



        return personList
    }
}