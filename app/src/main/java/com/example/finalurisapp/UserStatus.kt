package com.example.finalurisapp

data class UserStatus(
    val name : String = "",
    val url : String = ""
)
