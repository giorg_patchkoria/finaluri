package com.example.finalurisapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PasswordResetActivity : AppCompatActivity() {
    private lateinit var textEmail: EditText
    private lateinit var sendButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_reset)
        init()
        registerListeners()
    }
    private fun init(){
        textEmail = findViewById(R.id.edittext_email2)
        sendButton = findViewById(R.id.sendmain_button)
    }
    private fun registerListeners(){
        sendButton.setOnClickListener {
            val email = textEmail.text.toString()
            if(email.isEmpty()){
                Toast.makeText(this, "შეავსეთ ცარიელი ველი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance()
                .sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        Toast.makeText(this, "პაროლი გამოიგზავნა! შეამოწმე მეილი", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
}