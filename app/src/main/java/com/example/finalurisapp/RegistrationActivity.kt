package com.example.finalurisapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {
    private lateinit var editEmail: EditText
    private lateinit var editPassword: EditText
    private lateinit var registrationButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        init()
        registerListeners()
    }
    private fun init(){
        editEmail = findViewById(R.id.edittext_email1)
        editPassword = findViewById(R.id.edittext_password1)
        registrationButton = findViewById(R.id.registration_button1)
    }
    private fun registerListeners(){
        registrationButton.setOnClickListener {
            val email = editEmail.text.toString()
            val password = editPassword.text.toString()

            if(email.isEmpty() || password.isEmpty()){
                Toast.makeText(this, "შეავსეთ ყველა ველი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        startActivity(Intent(this,MainActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
}