package com.example.finalurisapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordActivity : AppCompatActivity() {
    private lateinit var textEmail: EditText
    private lateinit var sendButton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        init()
        registerListeners()
    }
    private fun init(){
        textEmail = findViewById(R.id.changepasswordedit_text)
        sendButton = findViewById(R.id.sendbutton3)
    }
    private fun registerListeners(){
        sendButton.setOnClickListener {
            val email = textEmail.text.toString()
            FirebaseAuth.getInstance()
                .sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful){
                        Toast.makeText(this, "პაროლი გამოიგზავნა, შეამოწმეთ მეილი", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

}