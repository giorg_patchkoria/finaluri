package com.example.finalurisapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment

class FirstFragment : Fragment(R.layout.fragment_first) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val Button = view.findViewById<Button>(R.id.button1)

        Button.setOnClickListener {
            startActivity(Intent(context, CarsActivity::class.java))
        }




    }

}