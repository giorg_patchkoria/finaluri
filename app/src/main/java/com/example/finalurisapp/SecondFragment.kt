package com.example.finalurisapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
val db = FirebaseDatabase.getInstance().getReference("UserInfo")
val auth = FirebaseAuth.getInstance()
class SecondFragment : Fragment(R.layout.fragment_second) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val imageView = view.findViewById<ImageView>(R.id.imageView2)
        val textViewName = view.findViewById<TextView>(R.id.textViewName)
        val editTextPhoto = view.findViewById<EditText>(R.id.editTextPhoto)
        val editTextName = view.findViewById<EditText>(R.id.editTextName)
        val saveButton = view.findViewById<Button>(R.id.savebutton)
        val changePasswordButton = view.findViewById<Button>(R.id.changePasswordButton)
        val logOutButton = view.findViewById<Button>(R.id.logOutButton)

        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userInfo: UserStatus = snapshot.getValue(UserStatus::class.java) ?: return
                textViewName.text = userInfo.name
                Glide.with(this@SecondFragment)
                    .load(userInfo.url)
                    .into(imageView)
            }


            override fun onCancelled(error: DatabaseError) {

            }

        })
        saveButton.setOnClickListener {
            val photo = editTextPhoto.text.toString()
            val name = editTextName.text.toString()

            val userInfo = UserStatus(photo, name)
            db.child(auth.currentUser?.uid!!)
                .setValue(userInfo)
        }
        changePasswordButton.setOnClickListener {
            startActivity(Intent(context, ChangePasswordActivity::class.java))
        }


        logOutButton.setOnClickListener(){
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(context, MainActivity::class.java))

        }
    }


}
